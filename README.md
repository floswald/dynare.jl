# WORK IN PROGRESS

## Installation 

### Adding packages 
```
using Pkg
Pkg.add(PackageSpec(url="https://git.dynare.org/julia-packages/fastlapackinterface.jl.git"))
Pkg.add(PackageSpec(url="https://git.dynare.org/julia-packages/polynomialmatrixequations.jl.git"))
Pkg.add(PackageSpec(url="https://git.dynare.org/julia-packages/linearrationalexpectations.jl.git"))
Pkg.add(PackageSpec(url="https://git.dynare.org/julia-packages/dynare.jl.git"))
```
or, interactively,
```
]add https://git.dynare.org/julia-packages/fastlapackinterface.jl.git
]add https://git.dynare.org/julia-packages/polynomialmatrixequations.jl.git
]add https://git.dynare.org/julia-packages/linearrationalexpectations.jl.git
]add https://git.dynare.org/julia-packages/dynare.jl.git
```
